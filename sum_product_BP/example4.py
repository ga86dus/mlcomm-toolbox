import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph
from fn_constraints import pc_array, or_array

x1 = variable_node('x1', 2)
x2 = variable_node('x2', 2)
x3 = variable_node('x3', 2)
x4 = variable_node('x4', 2)
x5 = variable_node('x5', 2)

f1 = factor_node('f1', np.array([0.2, 0.8]))
f2 = factor_node('f2', np.array([0.9, 0.1]))
f3 = factor_node('f3', np.array([0.3, 0.7]))
f4 = factor_node('f4', np.array([0.5, 0.5]))
f5 = factor_node('f5', np.array([0.2, 0.8]))

f13 = factor_node('f13', pc_array(2))
f124 = factor_node('f124', pc_array(2))
f235 = factor_node('f235', pc_array(3))

FG = factor_graph()
FG.add(f13)
FG.add(f124)
FG.add(f235)
FG.append('f13', x1)
FG.append('f13', x3)
FG.append('f124', x1)
#FG.append('f124', x2)
FG.append('f124', x4)
FG.append('f235', x2)
FG.append('f235', x3)
FG.append('f235', x5)

FG.append('x1' ,f1)
FG.append('x2' ,f2)
FG.append('x3' ,f3)
FG.append('x4' ,f4)
FG.append('x5' ,f5)

FG.compute_marginals(max_iter=10)
FG.brute_force()

print('p_{x_1} =',FG.nodes['x1'].marginal())
print('p_{x_1} =',FG.nodes['x1'].bfmarginal,'(bf)\n')
print('p_{x_2} =',FG.nodes['x2'].marginal())
print('p_{x_2} =',FG.nodes['x2'].bfmarginal,'(bf)\n')
print('p_{x_3} =',FG.nodes['x3'].marginal())
print('p_{x_3} =',FG.nodes['x3'].bfmarginal,'(bf)\n')
print('p_{x_4} =',FG.nodes['x4'].marginal())
print('p_{x_4} =',FG.nodes['x4'].bfmarginal,'(bf)\n')
print('p_{x_5} =',FG.nodes['x5'].marginal())
print('p_{x_5} =',FG.nodes['x5'].bfmarginal,'(bf)\n')

# print(or_array(4))
