#https://github.com/ilyakava/sumproduct
#@BOOK{barberBRML2012,
#author = {Barber, D.},
#title= {{Bayesian Reasoning and Machine Learning}},
#publisher = {{Cambridge University Press}},
#year = 2012}

import numpy as np

class Node:
    def __init__(self, name):
        self.connections = []
        self.inbox = {}
        self.name = name

    def append(self, to_node):
        self.connections.append(to_node)
        to_node.connections.append(self)

    def deliver(self, step_num, mu):
        if self.inbox.get(step_num):
            self.inbox[step_num].append(mu)
        else:
            self.inbox[step_num] = [mu]

class factor_node(Node):
    def __init__(self, name, potentials):
        self.p = potentials
        Node.__init__(self, name)

    def make_message(self, recipient):
        if not len(self.connections) == 1:
            unfiltered_mus = self.inbox[max(self.inbox.keys())]
            mus = [mu for mu in unfiltered_mus
                   if not mu.from_node == recipient]
            all_mus = [self.reformat_mu(mu) for mu in mus]
            lambdas = np.array([np.log(mu) for mu in all_mus])
            max_lambdas = np.nan_to_num(lambdas.flatten())
            max_lambda = max(max_lambdas)
            result = sum(lambdas) - max_lambda
            product_output = np.multiply(self.p, np.exp(result))
            return np.exp(
                np.log(self.summation(product_output, recipient)) + max_lambda)
        else:
            return self.summation(self.p, recipient)

    def reformat_mu(self, mu):  
        dims = self.p.shape
        states = mu.val
        which_dim = self.connections.index(mu.from_node)
        assert dims[which_dim] is len(states)

        acc = np.ones(dims)
        for coord in np.ndindex(dims):
            i = coord[which_dim]
            acc[coord] *= states[i]
        return acc

    def summation(self, p, node):  
        dims = p.shape
        which_dim = self.connections.index(node)
        out = np.zeros(node.size)
        assert dims[which_dim] is node.size
        for coord in np.ndindex(dims):
            i = coord[which_dim]
            out[i] += p[coord]
        return out

class variable_node(Node):
    def __init__(self, name, size):
        self.bfmarginal = None
        self.size = size
        Node.__init__(self, name)

    def marginal(self):
        if len(self.inbox):
            mus = self.inbox[max(self.inbox.keys())]
            log_vals = [np.log(mu.val) for mu in mus]
            valid_log_vals = [np.nan_to_num(lv) for lv in log_vals]
            sum_logs = sum(valid_log_vals)
            valid_sum_logs = sum_logs - max(sum_logs) 
            prod = np.exp(valid_sum_logs)
            return prod / sum(prod)
        else:
            return np.ones(self.size) / self.size

    def make_message(self, recipient):
        if not len(self.connections) == 1:
            unfiltered_mus = self.inbox[max(self.inbox.keys())]
            mus = [mu for mu in unfiltered_mus
                   if not mu.from_node == recipient]
            log_vals = [np.log(mu.val) for mu in mus]
            return np.exp(sum(log_vals))
        else:
            return np.ones(self.size)


class Mu:
    def __init__(self, from_node, val):
        self.from_node = from_node
        self.val = val.flatten() / sum(val.flatten())

class factor_graph:
    def __init__(self, first_node=None, debug=False):
        self.nodes = {}
        self.debug = debug
        if first_node:
            self.nodes[first_node.name] = first_node

    def add(self, node):
        assert node not in self.nodes
        self.nodes[node.name] = node

    def connect(self, name1, name2):
        self.nodes[name1].append(self.nodes[name2])

    def append(self, from_node_name, to_node):
        assert from_node_name in self.nodes
        tnn = to_node.name
        if not (self.nodes.get(tnn, 0)):
            self.nodes[tnn] = to_node
        self.nodes[from_node_name].append(self.nodes[tnn])
        return self

    def leaf_nodes(self):
        return [node for node in self.nodes.values()
                if len(node.connections) == 1]

    def export_marginals(self):
        return dict([
            (n.name, n.marginal()) for n in self.nodes.values()
            if isinstance(n, variable_node)
        ])

    @staticmethod
    def compare_marginals(m1, m2):
        assert not len(np.setdiff1d(m1.keys(), m2.keys()))
        return sum([sum(np.absolute(m1[k] - m2[k])) for k in m1.keys()])

    def compute_marginals(self, max_iter=500, tolerance=1e-6):  
        epsilons = [1]
        step = 0
        
        for node in self.nodes.values():
            node.inbox.clear()
        
        cur_marginals = self.export_marginals()
        
        for node in self.nodes.values():
            if isinstance(node, variable_node):
                message = Mu(node, np.ones(node.size))
                for recipient in node.connections:
                    recipient.deliver(step, message)

        while (step < max_iter) and tolerance < epsilons[-1]:
            last_marginals = cur_marginals
            step += 1
            factors = [n for n in self.nodes.values() if isinstance(n, factor_node)]
            variables = [n for n in self.nodes.values()
                         if isinstance(n, variable_node)]
            senders = factors + variables
            for sender in senders:
                next_recipients = sender.connections
                for recipient in next_recipients:
                    if self.debug:
                        print(sender.name + ' -> ' + recipient.name)
                    val = sender.make_message(recipient)
                    message = Mu(sender, val)
                    recipient.deliver(step, message)
            cur_marginals = self.export_marginals()
            epsilons.append(
                self.compare_marginals(cur_marginals, last_marginals))
        return epsilons[1:] 

    def brute_force(self):   
        variables = [v for v in self.nodes.values() if isinstance(v, variable_node)]

        var_dims = [v.size for v in variables]
        N = len(var_dims)
        assert N <= 32, "max number of vars for brute force is 32"
        log_joint_acc = np.zeros(var_dims)
        for factor in [f for f in self.nodes.values()
                       if isinstance(f, factor_node)]:
            
            which_dims = [variables.index(v) for v in factor.connections]
            factor_acc = np.ones(var_dims)
            for joint_coord in np.ndindex(tuple(var_dims)):
                factor_coord = tuple([joint_coord[i] for i in which_dims])
                factor_acc[joint_coord] *= factor.p[factor_coord]
            log_joint_acc += np.log(factor_acc)
        log_joint_acc -= np.max(log_joint_acc)
        joint_acc = np.exp(log_joint_acc) / np.sum(np.exp(log_joint_acc))
       
        for i, variable in enumerate(variables):
            sum_dims = [j for j in range(N) if not j == i]
            sum_dims.sort(reverse=True)
            collapsing_marginal = joint_acc
            for j in sum_dims:
                collapsing_marginal = collapsing_marginal.sum(j) 
            variable.bfmarginal = collapsing_marginal
        return variables
