import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph

x = variable_node('x', 2)

f1 = factor_node('f1', np.array([0.3, 0.7]))
f2 = factor_node('f2', np.array([1.0, 1.0]))

FG = factor_graph(debug=True)

FG.add(x)
FG.append('x', f1)
FG.append('x', f2)

FG.compute_marginals()
FG.brute_force()

print('p_x =',FG.nodes['x'].marginal())
print('p_x =',FG.nodes['x'].bfmarginal,'(bf)\n')
