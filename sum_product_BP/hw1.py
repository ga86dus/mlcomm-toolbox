## LDPC in biAWGN channel
import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph
from fn_constraints import pc_array
import time

PCM=np.array( [ # CCSDS (32,16)
        [3,4,6,9,15,17,24,29],
        [1,4,7,10,16,18,21,30],
        [1,2,8,11,13,19,22,31],
        [2,3,5,12,14,20,23,32],
        [1,5,6,9,13,17,21,25],
        [2,6,7,10,14,18,22,26],
        [3,7,8,11,15,19,23,27],
        [4,5,8,12,16,20,24,28],
        [4,5,9,11,13,21,26,29],
        [1,6,10,12,14,22,27,30],
        [2,7,9,11,15,23,28,31],
        [3,8,10,12,16,24,25,32],
        [3,5,9,13,16,17,25,29],
        [4,6,10,13,14,18,26,30],
        [1,7,11,14,15,19,27,31],
        [2,8,12,15,16,20,28,32] ] )

n = 32
k = 16

## define nodes
x = [variable_node('x'+str(i),2) for i in range(n)]
f = [factor_node('f'+str(i), pc_array(len(PCM[i]))) for i in range(n-k)]
obs = [factor_node('obs'+str(i), np.ones(2)) for i in range(n)]

## initialize factor graph
FG = factor_graph()
for i in range(n):
    FG.add(x[i])
    FG.append('x'+str(i), obs[i])

for i in range(n-k):
    FG.add(f[i])
    for j in range(len(PCM[i])):
        FG.connect('f'+str(i), 'x'+str(PCM[i][j]-1))

## parameter
SNRdB = 1

## (loop for SNRs can be added here)
ntx = 0
block_error = 0
bit_error = 0

while(block_error < 10):
    ntx += 1
    # cal delta
    scal = np.sqrt(np.power(10, (SNRdB/10)))
    # all zero codeword is a codeword for linear codes
    c = np.zeros(n)
    # mapping
    symbols = (1 - 2*c)*scal
    # AWGN
    N = np.random.normal(0, 1, n)
    y = symbols + N
    # demapping (# cal p(x_i|y_i))
    for i in range(n):
        obs[i].p = np.array([np.exp(-(y[i]-scal)**2/2)/np.sqrt(2*np.pi),
                             np.exp(-(y[i]+scal)**2/2)/np.sqrt(2*np.pi)])
        obs[i].p = obs[i].p/sum(obs[i].p) # normalization

    # running BP
    start_time = time.time()
    FG.compute_marginals(max_iter = 20)
    print("--- %s seconds ---" % (time.time()-start_time))
    # collect errors
    xhat = np.zeros(n)
    for i in range(n):
        if FG.nodes['x'+str(i)].marginal()[0] < 0.5:
            xhat[i] = 1

    ## print results
    bit_error += sum(xhat)
    if sum(xhat) != 0:
        block_error += 1
    print(sum(xhat),' Bit Errors in ntx =', ntx, ', Block Erros =', block_error)

## print results
print('BLER =', block_error/ntx, ', BER =', bit_error/(ntx*n), ', SNR =',str(SNRdB), 'dB')
