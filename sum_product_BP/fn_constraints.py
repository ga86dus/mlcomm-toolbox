import numpy as np

def pc_array(degree):
    size = np.ones(degree, dtype=np.int)*2
    out = np.zeros(size)
    for index,value in np.ndenumerate(out):
        if sum(index)%2 == 0:
            out[index] = 1
    return out

def or_array(degree):
    size = np.ones(degree + 1, dtype = np.int) * 2
    out = np.zeros(size)
    for index,value in np.ndenumerate(out):
        if not index[-1] == 0:
            out[index] = 1
        out[(0,)*degree] = [1., 0.]
    return out
