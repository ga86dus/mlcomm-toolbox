import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph
from fn_constraints import pc_array

x1 = variable_node('x1', 2)
x2 = variable_node('x2', 2)
x3 = variable_node('x3', 2)

f1 = factor_node('f1', np.array([0.1, 0.9]))
f2 = factor_node('f2', np.array([0.1, 0.9]))
f3 = factor_node('f3', np.array([0.2, 0.8]))

f4 = factor_node('f4', pc_array(3))

FG = factor_graph()

FG.add(f4)
FG.append('f4', x1)
FG.append('f4', x2)
FG.append('f4', x3)
FG.add(f1)
FG.connect('x1', 'f1')
#FG.append('x1', f1)
FG.append('x2', f2)
FG.append('x3', f3)

FG.compute_marginals()
FG.brute_force()

print('p_{x_1} =',FG.nodes['x1'].marginal())
print('p_{x_1} =',FG.nodes['x1'].bfmarginal,'(bf)\n')
