import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph

x1 = variable_node('x1', 2)
x2 = variable_node('x2', 2)
x3 = variable_node('x3', 2)
x4 = variable_node('x4', 2)

f1 = factor_node('f1', np.array([0.7, 0.3]))
f2 = factor_node('f2', np.array( [ [0.4,0.6],[0.5,0.5] ] ))
f3 = factor_node('f3', np.array( [ [0.8,0.2],[0.2,0.8] ] ))
f4 = factor_node('f4', np.array( [ [0.5,0.5],[0.7,0.3] ] ))

FG = factor_graph(debug=False)

FG.add(f1)
FG.append('f1', x1)
FG.append('x1', f2)
FG.append('f2', x2)
FG.append('x2', f3)
FG.append('f3', x3)
FG.append('x3', f4)
FG.append('f4', x4)

FG.compute_marginals()
FG.brute_force()

print('p_{x_3} =',FG.nodes['x3'].marginal())
print('p_{x_3} =',FG.nodes['x3'].bfmarginal,'(bf)\n')
