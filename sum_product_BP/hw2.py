# sample testing by pooling
import numpy as np
from sum_product_BP import variable_node, factor_node, factor_graph
from fn_constraints import or_array
import time

pools = np.array( [
        [1,4,7],
        [2,5,8],
        [3,6,9],
        [1,6,8],
        [2,4,9],
        [3,5,7],
        [1,5,9],
        [2,6,7],
        [3,4,8],
        [1,2,3],
        [4,5,6],
        [7,8,9] ] )

k = 9
n = 12
a_pri = np.array([0.8, 0.2]) # a prior information
p_t = np.array([0.9, 0.1]) # crossover probability of BSC

## define nodes
x = [variable_node('x'+str(i), 2) for i in range(k)]
p = [variable_node('p'+str(i), 2) for i in range(n)]
ap = [factor_node('ap'+str(i), a_pri) for i in range(k)]
obs = [factor_node('obs'+str(i), np.ones(2)) for i in range(n)]
or_nodes = [factor_node('or_nodes'+str(i),
                        or_array(len(pools[i]))) for i in range(n)]

## initialize factor graph
FG = factor_graph()

for i in range(k):
    FG.add(x[i])
    FG.append('x'+str(i), ap[i])

for i in range(n):
    FG.add(or_nodes[i])
    for j in range(len(pools[i])):
        FG.connect('or_nodes'+str(i), 'x'+str(pools[i][j]-1))
    FG.append('or_nodes'+str(i), p[i])

for i in range(n):
    FG.append('p'+str(i), obs[i])

## simulation begin
error = 0
t = 0
while(error < 100):
    t += 1
    # source
    u = np.random.choice([0, 1], size=(k,), p=a_pri)
    # enc
    c = np.zeros(n, dtype=np.int)
    for i in range(n):
        for j in range(len(pools[i])):
            c[i] = c[i] or u[ pools[i][j]-1 ]
    # channel
    y = np.array(np.logical_xor(c,
                        np.random.choice([0, 1], size=(n,), p=p_t)),dtype=int)
    # cal p(y_i|x_i)
    for i in range(n):
        if y[i] == 0:
            obs[i].p = p_t
        else:
            obs[i].p = 1 - p_t
    # running BP
    start_time = time.time()
    FG.compute_marginals()
    print("--- %s seconds ---" % (time.time() - start_time))
    # collect errors
    xhat = np.zeros(n, dtype=int)
    for i in range(k):
        if FG.nodes['x'+str(i)].marginal()[0] < 0.5:
            xhat[i] = 1
        if not xhat[i] == u[i]:
            error += 1
            print('Bit Errors =',error)

## print results
print('Error Rate =', error / (t*k))
