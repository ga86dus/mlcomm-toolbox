import numpy as np
import numpy.linalg as lin
import matplotlib.pyplot as plt
from numpy.linalg import svd
from numpy.random import randn
from sklearn.preprocessing import scale

# 1. 2. pca function implementation
def pca(x, param, mode = "preserve_var", c_mode = "f_wise", norm = "none"):
	# input: x: data matrix in R^(M*N); M dimension, N samples
	#        param: target dimention K or target conserved variance \alpha
	#        mode: target dimention or target conserved variance ratio
	#        c_mode: the mode of the mean shifting
	#        norm: the mode of the normalization to unit variance
	#              f_wise:feature wise;
	#              s_wise:sample wise;
	#              none: no shift/variance normalization

	if c_mode == "f_wise":
		mu = np.mean(x, axis = 1)
		X = scale(x, axis = 1, with_std = False)
	elif c_mode == "n_wise":
		mu = np.mean(x, axis = 0)
		X = scale(x, axis = 0, with_std = False)
	else:
		pass

	if norm == "f_wise":
		sigma = np.std(x, axis = 1)
		X = scale(X, axis = 1, with_mean = False)
	elif norm == "s_wise":
		sigma = np.std(x, axis = 0)
		X = scale(X, axis = 0, with_mean = False)
	else:
		pass

	C_X = X.dot(X.T)/X.shape(1)
	ei_vec, s, v = svd(C_X)
	ei_val = s**2
    
	if mode == "preserve_var":
		# Compute K
		ei_val_sum = sum(ei_val)
		K = 1
		while (sum(ei_val[:K])/ei_val_sum < param):
			K =+ 1
	else:
		K = param

	W = ei_vec[:,:K]
	Y = W.T.dot(x)

	return (Y,W,mu,sigma)


# 3. data generation
P = np.array([[0.5, -0.866],[0.866, 0.5]])
X = P.dot(np.array([[1],[4]])*randn(2,1000)+np.array([[1],[0.5]]))

# 4. scatter plot of the data
plt.figure(1)
plt.plot(X[0,:], X[1,:], 'b.')

# 5. Choose an appropriate mode for mean shift and variance normalization and per-
# form PCA on the given dataset.
Y, W, mu, sigma = pca(X, 0.9, mode = "preserve_var", c_mode = "f_wise", norm = "f_wise")

# 6. Plot the principal components into your scatterplot. Check if your results make sense.
plt.plot(X[0, :], Y[0,:], 'r.')
plt.plot([-10*W[0], -10*W[1]], [10*W[0], 10*W[1]], 'g-')
plt.show()

# 7. Reduce the dimensionality to one and plot the projected data into another plot.
plt.figure(2)
plt.plot(X[0,:], X[1,:], 'b.')

plt.plot(Y[0,:], np.zeros((1, 1000))[0,:], 'r.')
plt.plot([-10*W[0], -10*W[1]], [10*W[0], 10*W[1]], 'g-')
plt.show()
