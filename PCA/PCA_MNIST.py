import numpy as np
import numpy.linalg as lin
import struct
import matplotlib.pyplot as plt
from numpy.linalg import svd
from numpy.random import randn
from sklearn.preprocessing import scale

def read_mnist(filename, mode="images"):

	# Load everything in some numpy arrays
	

	if mode = "labels":
		with open(filename, 'r') as f_read:
			magic, num = struct.unpack(">II", f_read.read(8))
			X = np.fromfile(f_read, dtype = np.int8)

	# elif mode = "images":
	# 	with open(filename, 'rb') as fimg:
	# 		magic, num, rows, cols = struct.unpack(">IIII", fimg.read(16))
	# 		img = np.fromfile(fimg, dtype=np.uint8).reshape((rows, cols, ))

 #    get_img = lambda idx: (lbl[idx], img[idx])

 #    # Create an iterator which returns each image in turn
 #    for i in xrange(len(lbl)):
 #        yield get_img(i)

	return X



def read(dataset = "training", path = "."):

	if dataset is "training":
        fname_img = os.path.join(path, 'train-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 'train-labels.idx1-ubyte')
    elif dataset is "testing":
        fname_img = os.path.join(path, 't10k-images.idx3-ubyte')
        fname_lbl = os.path.join(path, 't10k-labels.idx1-ubyte')
    else:
        raise ValueError, "dataset must be 'testing' or 'training'"

    

def show(image):
    """
    Render a given numpy.uint8 2D array of pixel data.
    """
    from matplotlib import pyplot
    import matplotlib as mpl
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    imgplot = ax.imshow(image, cmap=mpl.cm.Greys)
    imgplot.set_interpolation('nearest')
    ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_ticks_position('left')
    pyplot.show()