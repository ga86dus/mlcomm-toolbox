# -*- coding: utf-8 -*-
import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
import mlcomm.nn.utils as nnu
from dataset2_logreg import DataSet  

#get data
y_D, x_D = DataSet.get_data()
DataSet.plot_data()
plt.show()

# --------parameters--------
nonlinear_degree = 8
iterations = 30000
learning_rate = 0.1
lambd = 0.1 # penalty para: make the model simple

#extend x
x_D = nnu.poly_extend_data2D(x_D, nonlinear_degree)

#random init of weights 
### YOUR CODE HERE ###
w = np.random.normal(0, 1, (x_D.shape[0], 1))


#normalization:
x_D, norm_param = nnu.normalize_data(x_D)


#plot and print cost
def extension_wrapper(x):
    return nnu.poly_extend_data2D(x, nonlinear_degree)
DataSet.plot_decision_boundary(w, extension_wrapper, norm_param)
plt.show()
print('Cost:%f' % nnu.lor_cost(w, y_D, x_D, lambd))

#compute gradient and do gradient descent
def gradient_wrapper(w):
    return nnu.lor_grad(w, y_D, x_D, lambd)
w = nnu.gradient_descent(iterations, learning_rate, w, gradient_wrapper)


#plot and print cost
DataSet.plot_decision_boundary(w, extension_wrapper, norm_param)
plt.show()
print('Cost:%f' % nnu.lor_cost(w, y_D, x_D, lambd))