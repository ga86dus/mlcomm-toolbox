import sys
sys.path.append('../')

import numpy as np
from numpy.random import randn
import matplotlib.pyplot as plt
from mlcomm.dim.dim import pca, pca_reconstruct

np.random.seed(0)
P = np.array([[0.5, -0.866],[0.866, 0.5]])
X_raw = P.dot(np.array([[1],[4]])*randn(2,1000)+np.array([[1],[0.5]]))

"""
Plot raw data 
"""
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(X_raw[0,:],X_raw[1,:])
ax.set_aspect('equal')
plt.title("raw data set")
plt.show()

"""
PCA without normalization and mean shift
"""
k = 2
(Y,W,mu,sigma) = pca(X_raw,k,mode="fixed_k",c_mode="none")

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Y[0,:],Y[1,:])
ax.set_aspect('equal')
plt.title("PCA without normalization, mean shift and reduction")
plt.show()

"""
PCA with feature-wise mean shift (covariance matrix) and variance normalization
"""
k=2
(Y,W,mu,sigma) = pca(X_raw,k,mode="fixed_k",c_mode="f_wise",norm="f_wise")

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Y[0,:],Y[1,:])
ax.set_aspect('equal')
plt.title("PCA with f-wise mean shift and normalization")
plt.show()

"""
Plotting principal components
"""
X = (X_raw - mu)/sigma
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(X[0,:],X[1,:])
plt.arrow(0,0,W[0,0],W[1,0])
plt.arrow(0,0,W[0,1],W[1,1])
ax.set_aspect('equal')
plt.title("centralized and normalized data with principal components")
plt.show()

"""
Reduce the dimensionality to one
"""
fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(Y[0,:],np.zeros((1,1000)))
plt.title("data reduced to the first principal component")
plt.show()


"""
Reconstruct two dimensional data from one dimension
"""
X_hat = pca_reconstruct(np.reshape(Y[0,:],(1,1000)),np.reshape(W[:,0],(2,1)),mu,sigma)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(X_hat[0,:],X_hat[1,:])
ax.set_aspect('equal')
plt.title("Reconstruction of the data")
plt.show()