#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
import numpy as np
import scipy as sc
from mlcomm.dim.dim import pca, pca_reconstruct
from mlcomm.tools.mnist import read_mnist, show_mnist

"""
Read MNIST dataset and show original images
"""

(X,img_size) = read_mnist('train-images-idx3-ubyte',mode="images")
show_mnist(X[:,0:100],img_size)

"""
PCA with K=40 for MNIST with reconstruction 
"""
K = 40

# no mean-shift, no variance normalization
(Y,U,mu,sigma) = pca(X,K,mode="fixed_k",c_mode="none")
X_hat = pca_reconstruct(Y,U,mu,sigma)
err = np.mean(np.square(X_hat-X))
print("Reconstruction Error without mean-shift and without variance normalization: {:.2f}".format(err))
show_mnist(X_hat[:,0:100],img_size)


# feature wise mean-shift, no variance normalization
(Y,U,mu,sigma) = pca(X,K,mode="fixed_k",c_mode="f_wise")
X_hat = pca_reconstruct(Y,U,mu,sigma)
err = np.mean(np.square(X_hat-X))
print("Reconstruction Error with feature wise mean-shift and without variance normalization: {:.2f}".format(err))
show_mnist(X_hat[:,0:100],img_size)


# sample wise mean-shift, no variance normalization
(Y,U,mu,sigma) = pca(X,K,mode="fixed_k",c_mode="s_wise")
X_hat = pca_reconstruct(Y,U,mu,sigma)
err = np.mean(np.square(X_hat-X))
print("Reconstruction Error with sample wise mean-shift and without variance normalization: {:.2f}".format(err))
show_mnist(X_hat[:,0:100],img_size)


# feature wise mean-shift, feature wise variance normalization
(Y,U,mu,sigma) = pca(X,K,mode="fixed_k",c_mode="f_wise",norm="f_wise")
X_hat = pca_reconstruct(Y,U,mu,sigma)
err = np.mean(np.square(X_hat-X))
print("Reconstruction Error with feature wise mean-shift and variance normalization: {:.2f}".format(err))
show_mnist(X_hat[:,0:100],img_size)


# sample wise mean-shift, sample wise variance normalization
(Y,U,mu,sigma) = pca(X,K,mode="fixed_k",c_mode="s_wise",norm="s_wise")
X_hat = pca_reconstruct(Y,U,mu,sigma)
err = np.mean(np.square(X_hat-X))
print("Reconstruction Error with sample wise mean-shift and variance normalization: {:.2f}".format(err))
show_mnist(X_hat[:,0:100],img_size)













