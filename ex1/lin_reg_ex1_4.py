# -*- coding: utf-8 -*-

# ex1 : 1.4
import sys
import numpy as np
import matplotlib.pyplot as plt
sys.path.append('../')
import mlcomm.nn.utils as nnu
from dataset2_linreg import DataSet  

#get and plot the data
y_D,x_D = DataSet.get_data()
DataSet.plot_data()
plt.show()

# --------parameters--------
nonlinear_degree = 8
iterations = 50
learning_rate = 0.015

#extend x with ones:
x_D = nnu.sin_extend_data1D(x_D, nonlinear_degree)


#random init of w:
### YOUR CODE HERE ###
w = np.random.normal(0, 1, (x_D.shape[0], 1))

#normalization:
x_D, norm_param = nnu.normalize_data(x_D)


#plot and compute cost
def extension_wrapper(x):
    return nnu.sin_extend_data1D(x, nonlinear_degree)

DataSet.plot_model(w, extension_wrapper, norm_param)
plt.show()
print('Cost:%f' % nnu.lir_cost(w, y_D, x_D))


#compute gradient and do gradient descent
def gradient_wrapper(w):
    return nnu.lir_grad(w, y_D, x_D)
w = nnu.gradient_descent(iterations, learning_rate, w, gradient_wrapper)


#plot and compute cost
DataSet.plot_model(w, extension_wrapper, norm_param)
plt.show()
print('Cost:%f' % nnu.lir_cost(w, y_D, x_D))

