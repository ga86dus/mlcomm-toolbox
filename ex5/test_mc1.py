#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
import numpy as np
from scipy.integrate import quad
import scipy.stats
import matplotlib.pyplot as plt
from mlcomm.em.importance_sampling import importance_sampling
from mlcomm.em.rejection_sampling import rejection_sampling
from test_px import Px


############################################
# Task 2: Importance and Rejection Sampling
############################################





