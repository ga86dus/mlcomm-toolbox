#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

def Px(x):
    return np.exp(0.4*(x-0.4)**2 -0.08*x**4)