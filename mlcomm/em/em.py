# -*- coding: utf-8 -*-

import numpy as np
import scipy.stats as sc

def kmeans(y, C0, iters):
    """
    Calculates the clusters of the observations stored in 'y'
    performing 'iters' iterations and using an initial cluster C0.
    The latter can be chosen at random from the provided samples.

    Returns the newly calculated centroids.
    """
    N = y.size
    K = C0.size

    C = C0
    it = 0

    while it < iters:

        tmp = np.abs(y.reshape((y.size,1))-C)
        tmp_idx = np.argmin(tmp,axis=1)

        for j in range(K):
            idx = (tmp_idx == j)
            C[j] = np.sum(y[idx])/np.sum(idx)

        it = it + 1


    return C

def kmeans_multidim(y, C0, iters):
    """
    Calculates the clusters of the observations stored in 'y'
    performing 'iters' iterations and using an initial cluster C0.
    The latter can be chosen at random from the provided samples.

    Returns the newly calculated centroids.

    Multidimensional version, i.e., assumes y to be of the form 
    y = [y_1 y_2 ... y_N], where y_i is a N-dimensional vector.
    Similarly, C0 = [c0_1, c0_2, ..., c0_K] and c0_i is N-dimensional.
    """
    M = y.shape[0]
    N = y.shape[1]
    K = C0.shape[1]

    C = C0
    it = 0

    tmp_idx = np.zeros(N)

    while it < iters:


        for i in np.arange(N):
            tmp = np.linalg.norm(C-y[:,i].reshape((M,1)),axis=0)
            tmp_idx[i] = tmp.argmin()

        for j in np.arange(K):
            idx = (tmp_idx == j)
            C[:,j] = np.mean(y[:,idx],axis=1)

        it = it + 1
    return (C,tmp_idx)

def kmeans_assign(y,C):
    # assigns new data y to a given cluster C
    if C.shape[0] != y.shape[0]:
        raise ValueError('data has to have same dimensionality as cluster')
    N = y.shape[1]
    M = C.shape[0]
    K = C.shape[1]

    idx = np.zeros(N)
    for i in np.arange(N):
        tmp = np.linalg.norm(C-y[:,i].reshape((M,1)),axis=0)
        idx[i] = tmp.argmin()

    return idx

def kmeans_label(y,label,C):
    (M, number_clusters) = C.shape

    idx = kmeans_assign(y, C)
    
    cluster_labels = np.zeros(number_clusters)

    for i in range(number_clusters):
        counts = np.bincount(label[idx==i])
        cluster_labels[i] = np.argmax(counts)

    return cluster_labels.astype(int)

def em(y, x_in, px_in, sigma2_in, iter):
    """
    Implements the expectation maximization algorithm for 
    a Gaussian mixture model. y should be a numpy vector
    containing the observed samples. The variables x_in 
    and px_in represent the initial start values for the 
    constellations points and probability distribution, 
    respectively. 'iter' defines the number of iterations.

    A dictionary is returned with the fields 'x', 'px'
    and 'sigma2'.
    """

    N = y.size
    px_out = px_in.reshape((1,px_in.size))
    x_out = x_in.reshape((1,x_in.size))
    sigma2_out = sigma2_in
    
    y = y.reshape((N,1))

    it = 0
    while it < iter:

        # E-Step: calculate the posterior distributions Q_X^i(x) for all samples y_i
        tmp = sc.norm.pdf(y, loc=x_out, scale=np.sqrt(sigma2_out)) * px_out
        p_xy = tmp / np.sum(tmp, axis=1).reshape((N,1)) # use broadcasting here!

        # M-step: maximization the ELBO over the parameters; the following 
        # equations are the closed form expressions for the hereby obtained
        # stationary points
        px_out = np.sum(p_xy, axis=0)/N
        x_out = np.sum(y * p_xy, axis=0) / np.sum(p_xy, axis=0) # use broadcasting here!
        x_out = x_out.reshape((1,x_out.size))
        sigma2_out = np.mean(np.sum(p_xy * (y - x_out)**2, axis=1)) # use broadcasting here!

        it = it + 1


    data_out = {}
    data_out['x'] = x_out[0,:]
    data_out['px'] = px_out
    data_out['sigma2'] = sigma2_out

    return data_out

