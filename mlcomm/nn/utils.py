# -*- coding: UTF-8 -*-

import numpy as np

def lor_cost(w, y, x, lambd = 0):
    """
    Computes cost for logistic regression with parameters w and data set x,y
    y = np.array of size 1xN
    x = np.array of size MxN
    w = np array of size Mx1
    Output:
    cost = scalar
    """
    ## YOUR CODE HERE ###
    # use flags to mark the value of y=1 or y=0 
    # in order to ignore them in a certain case
    z = np.matmul(w.T, x)# w.T.dot(x)
    a = act_fct(z, "sigmoid")
    flag_yeq1 = np.nonzero(y == 1)[1]
    flag_yeq0 = np.nonzero(y == 0)[1]
    cost = np.sum(-np.log(a[:, flag_yeq1])) + np.sum(-np.log(1 - a[:, flag_yeq0]))
    cost += lambd * np.sum(w ** 2)
    #####################
    return cost
    
def lor_grad(w, y, x, lambd = 0): 
    """
    Returs gradient for logistic regression with the cross entropy cost function 
    for parameter w and data set y, x.
    y = np.array of size 1xN
    x = np.array of size MxN
    w = np array of size Mx1
    Output:
    gradT = np array of size Mx1
    """
    ## YOUR CODE HERE ###
    z = np.matmul(w.T, x)
    a = act_fct(z, "sigmoid")
    # gradT = np.sum(np.multiply(a - y, x), axis = 1)
    gradT = x.dot((a - y).T)
    gradT += 2 * lambd * w
    #####################
    return gradT

def poly_extend_data2D(x, p=1):
    """
    Extend the provided input matrix x wtih all subsequent powers of terms of the input.
    x = np.array of size 2xN
    Output:
    x_e = np.array 
    Eg. for p=3 and x of dimensions 2xN. x_e should be a matrix such that 
    the 1st row is [1 1 .. 1], 2nd X[0,:], 3rd X[1,:], 4th X[0,:]**2,
    5th X[0,:]*X[1,:], 6th X[1,:]*2, 7th X[0,:]**3,  8th X[0,:]**2*X[1,:], 
    and so on... till 10th row equal X[1,:]**3 
    """      
    ### YOUR CODE HERE ###
    #x_e = np.vstack([np.ones((1,x.shape[1])),x[0,:],x[1,:]])  
    x0 = x[0, :]
    x1 = x[1, :]
    x_e = np.vstack([x0 ** i * x1 ** (d-i) for d in range(0, p + 1) for i in range(0, d + 1)])
    ### ######### ###    
    return x_e 

def normalize_data(x):
    """
    Normalizes data. Should not normalize the first row (we assume it is the row of ones).
    x = np.array of size MxN
    Output:
    x_norm     = normalized np.array of size MxN    
    norm_param = distionary with two keys "mean" and "var". Each key contains 
    a np.array of size Mx1 with the mean and variance of each row of data array. 
    For the first row,  set mean=0 and var=1
    """
    ### YOUR CODE HERE ###
    x_mean = np.mean(x, axis = 1)
    x_std = np.std(x, axis = 1)
    x_norm = x
    #x_norm[1:, :] = (x[1:, :] - x_mean[1:].reshape((-1,1))) / x_std[1:].reshape((-1,1))
    x_norm[1:, :] = np.transpose(np.divide(np.transpose(x[1:, :]) - x_mean[1:], x_std[1:])) 
    x_mean[0] = 0
    x_std[0] = 1
    norm_param = {"mean" : x_mean, "var" : np.square(x_std)}
	########################
    return x_norm, norm_param


def lir_grad(w, y, x): 
    """
    Returns gradient for linear regression with quadratic cost for parameter w and data set y, x.
    y = np.array of size 1xN
    x = np.array of size MxN
    w = np array of size Mx1
    Output:
    gradT = np array of size Mx1
    """
    ### YOUR CODE HERE ###
    lin_out = np.matmul(w.T, x) # a = w.T.dot(x)
    gradT = x.dot((lin_out - y).T)
    # gradT = np.sum(np.multiply(lin_out - y, x), axis = 1) # 
    # gradT = gradT.reshape((-1,1))
	######################
    return gradT



def gradient_descent(iter_num, l_rate, w_0, gradient_func):
    """
    Performs gradient descent for iter_num iterations with learning rate l_rate from initial
    position w_0. 
    w_0 = np array of size Mx1
    gradient_func(w) is a function which returns gradient for parameter w
    Output:
    w_opt = optimal parameters
    """
    ### YOUR CODE HERE ###
    n = iter_num
    w_old = w_0
    while n > 0:
        n = n - 1
        w_opt = w_old - l_rate * gradient_func(w_old)
        w_old = w_opt
    ######################
    return w_opt


def poly_extend_data1D(x, p):
    """
    Extend the provided input vector x, wtih subsequent powers of the input.
    x = np.array of size 1xN
    Output:
    x_e = np.array of size (p+1)xN such that 1st row = x^0, 2nd row = x^1, ...
    """      
    ### YOUR CODE HERE ###
    #print("p = " + str(p))
    x_e = np.vstack([x ** i for i in range(0, p + 1)]) # vertical stack
	########################
    return x_e

def sin_extend_data1D(x, p):
    """
    Extend the provided input vector x, wtih P subsequent sin harmonics of the input.
    x = np.array of size 1xN
    Output:
    x_e = np.array of size (p+1)xN
    """      
    ### YOUR CODE HERE ###
    x_max = np.amax(x)#, axis = 1)
    x_div_max = np.transpose(np.divide(np.transpose(x), x_max))
    freq = np.vstack([x_div_max * i for i in range(0, p + 1)])
    x_e = freq
    x_e[0, :] = np.ones((1, x.shape[1]))
    x_e[1:, :] = np.sin(2 * np.pi * freq[1:, :])
    ######################    
    return x_e 

def lir_cost(w, y, x):
    """
    Computes cost for linear regression with parameters w and data set x,y
    y = np.array of size 1xN
    x = np.array of size MxN
    w = np array of size Mx1
    Output:
    cost = scalar
    """
    ## YOUR CODE HERE ###
    lin_out = np.matmul(w.T, x)
    cost = np.sum(np.square((y - lin_out))/ 2) # be able to use ** directly
    #####################
    return cost

def act_fct(x, type_fct):
    """
    Implements different activation functions to be used in Neural Networks. The
    variable x is the function parameter and type_func defines which functions should
    be chosen, i.e., y = f(x). Valid choices are
    
    'identity': y = f(x) = x
    'sigmoid': y = f(x) = 1/(1+exp(-x))
    'tanh': y = f(x) = tanh(x)
    'rect_lin_unit': y = f(x) = max(x,0)


    """
    if type_fct not in ['identity', 'sigmoid', 'tanh', 'rect_lin_unit']:
        raise ValueError('activation function type {} is not known'.format(type_fct))
    
    x = np.asarray(x, dtype=float)

    if type_fct == 'identity':
        y = x
    elif type_fct == 'sigmoid':
        y = 1/(1+np.exp(-x))
    elif type_fct == 'tanh':
        y = np.tanh(x)
    elif type_fct == 'rect_lin_unit':
        y = np.max(np.vstack((x, np.zeros(x.shape))), axis=0)

    return y
    
def dact_fct(x, type_fct):
    """
    Implements derivatives of  activation functions to be used in Neural Networks. The
    Inputs:
        x = np.array of input values
        type_act = 
             'identity' : for activation  y = f(x) = x
             'sigmoid': for activation y = f(x) = 1/(1+exp(-x))
             'tanh': for activation y = f(x) = tanh(x)
             'rect_lin_unit': for activation y = f(x) = max(x,0)
    Output:         
       y = np.array containing f'(x)    
    """
    y = None
    ###YOUR CODE HERE###
    if type_fct not in ['identity', 'sigmoid', 'tanh', 'rect_lin_unit']:
        raise ValueError('activation function type {} is not known'.format(type_fct))
    
    x = np.asarray(x, dtype=float)

    if type_fct == 'identity':
        y = np.ones(x.shape)
    elif type_fct == 'sigmoid':
        y = act_fct(x, type_fct) * (1 - act_fct(x, type_fct))
        # y = np.exp(-x) / ((1 + np.exp(-x)) ** 2)
    elif type_fct == 'tanh':
        y = 1 - np.tanh(x) ** 2 # 1 - tanh^2
    elif type_fct == 'rect_lin_unit':
        y = np.max(np.vstack((x, np.zeros(x.shape))), axis=0)
        x_pos = np.nonzero(x>0)[1]
        y[x_pos] = 1
    ##################    
    return y