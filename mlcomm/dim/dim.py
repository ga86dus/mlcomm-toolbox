import numpy as np 
from numpy import linalg

def pca(X,param,mode="preserve_var",c_mode="f_wise",norm="none"):

    (M,N) = X.shape

    """
    Mean Shift
    """
    if c_mode is "f_wise":
        mu = np.reshape(np.mean(X,axis=1),(M,1))
    elif c_mode is "s_wise":
        mu = np.reshape(np.mean(X,axis=0),(1,N))
    else:
        mu = 0

    X = X - mu


    """
    Variance normalization
    """
    eps = 0.001
    if norm is "f_wise":
        sigma = np.reshape(np.sqrt(np.var(X,axis=1)),(M,1))
        zero_flags = sigma <= eps # if variance is close to zero 
        sigma[zero_flags] = 1
    elif norm is "s_wise":
        sigma = np.reshape(np.sqrt(np.var(X,axis=0)),(1,N))
        zero_flags = sigma <= eps
        sigma[zero_flags] = 1
    else:
        sigma = 1

    X = X/sigma

    """
    Compute Covariance Matrix
    """
    C_x = X.dot(X.T)/(N-1)

    """
    SVD
    """
    U, s, V = linalg.svd(C_x)


    """
    Choose the target dimension K
    """
    if mode is "preserve_var":
        for i in range(0,M):
            if np.sum(s[0:i])/np.sum(s) >= param:
                K = i
                break
    elif mode is "fixed_k":
        K = param
    else:
        raise ValueError('mode must be "preserve_var" or "fixed_k"')

    W = U[:,0:K]
    Y = W.T.dot(X)

    return (Y,W,mu,sigma)


def pca_reconstruct(Y,W,mu,sigma):
    X = W.dot(Y)
    X = X*sigma
    X = X+mu
    return X
