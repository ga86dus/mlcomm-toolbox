import struct
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def read_mnist(filename,mode="images"):
    with open(filename,'rb') as f:
        if mode is "images":
            magic, num, rows, cols = struct.unpack(">IIII", f.read(16))
            X = np.fromfile(f, dtype=np.uint8).reshape((num,rows*cols)).T 
            X = X.astype(float)
            return (X,(rows,cols))
        elif mode is "labels":
            magic, num = struct.unpack(">II", f.read(8))
            X = np.fromfile(f, dtype=np.int8)
            return X
        else: 
            raise ValueError('mode must be "images" or "labels"')



def show_mnist(X,img_size):

    if X.ndim == 1:
        X = X.reshape((len(X),1))
    N = X.shape[1]

    num_columns = int(np.ceil(np.sqrt(N)))
    num_rows = int(np.ceil(N/num_columns))

    images = X.T.reshape(N,img_size[0],img_size[1])

    idx = 0
    img = np.array([])
    for row in range(0,num_rows):
        for column in range(0,num_columns):
            if column == 0:
                new_row = images[idx,:,:]
                idx += 1
            elif idx >= N:
                new_row = np.hstack((new_row,np.zeros(img_size)))
            else:
                new_row = np.hstack((new_row,images[idx,:,:]))
                idx += 1
        
        if row == 0:
            img = new_row
        else:
            img = np.vstack((img,new_row))

    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    imgplot = ax.imshow(img, cmap=mpl.cm.Greys)
    plt.show()