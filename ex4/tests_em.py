#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
from mlcomm.em.em import em
from mlcomm.tools.stats import awgn_pY


M = 8
num = 100000
SNRdB = 12
SNR = 10**(SNRdB/10)


X = np.arange(-M+1,M+1,2)
nu = 0.0641 # chosen to get a distribution with a selected entropy of 2.5
pX = np.exp(-nu*X**2)
pX = pX/np.sum(pX)

x = np.random.choice(X, p=pX, size=num)

P = np.mean(x**2)
sigma2 = P/SNR

y = x + np.random.randn(num) * np.sqrt(sigma2)

# show the density plot of Y, i.e., p_Y(y)
xx = np.linspace(np.min(x)-1, np.max(x)+1, 1000)
real_py = awgn_pY(xx,pX,X,sigma2)
plt.plot(xx, real_py)
plt.show()

# EM
params_out = em(y, np.linspace(np.min(y), np.max(y), 8), np.ones((1,8))/8, 1, 10)
print(params_out)

print("Sigma2: {:.3f}".format(params_out['sigma2']))
print("P_X: {:.2e}, {:.2e}, {:.2e}, {:.2e}, {:.2f}, {:.2f}, {:.2f}, {:.2f}".format(*params_out['px']))
print("X: {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}".format(*params_out['x']))


