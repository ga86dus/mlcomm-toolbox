#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
from mlcomm.em.em import kmeans


M = 8
num = 100000
SNRdB = 15
SNR = 10**(SNRdB/10)


X = np.arange(-M+1,M+1,2)
#nu = 0.0641 # chosen to get a distribution with a selected entropy of 2.5
#pX = np.exp(-nu*X**2)
#pX = pX/np.sum(pX)
pX = np.ones(M)/M

x = np.random.choice(X, p=pX, size=num)

P = np.mean(x**2)
sigma2 = P/SNR

y = x + np.random.randn(num) * np.sqrt(sigma2)


# K-Means
clusters = kmeans(y, np.linspace(np.min(y), np.max(y), 8), 100)

print("K-Means Clusters: {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}, {:.2f}".format(*clusters))